# plusbasic theme

clone of boet's bluebasic theme at https://github.com/BOETZILLA/BLUEBASIC_Theme
which is based on hubzilla's redbasic, but not directly forked from it.

some of us are trying to create a welcome-hub for plussers leaving G+ and this
theme is an attempt to make it easier for them to find their way around.

to install it go to the hub's webroot and issue the following command:

<code>util/add_theme_repo https://github.com/phani00/PLUS_basic.git plusbasic</code>
